/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m03.uf5.p01.grup8.gestiohospital;

import java.util.List;

/**
 *
 * @author Christian
 */
public class Pacient extends Persona {

    public Pacient(String nom, String cognom1, String cognom2, String numSegSocial, String NIF, String telefon) {
        super(nom, cognom1, cognom2, numSegSocial, NIF, telefon);
        super.nom = nom;
        super.cognom1 = cognom1;
        super.cognom2 = cognom2;
        super.numSegSocial = numSegSocial;
        super.NIF = NIF;
        super.telefon = telefon;
    }

    public static void mostrarPacient(int index, List<Pacient> pacient) {
        System.out.println("-----------------Informacio del Usuari----------------------");
        System.out.println("Pacient Nom " + pacient.get(index).getnom());
        System.out.println("Pacient Cognom " + pacient.get(index).getcognom1());
        System.out.println("Pacinet Segon cognom " + pacient.get(index).getcognom2());
        System.out.println("Numero de la seguretar social " + pacient.get(index).getnumSegSocial());
        System.out.println("Numero NIF " + pacient.get(index).getNIF());
        System.out.println("Telefon " + pacient.get(index).gettelefon());
    }

}
