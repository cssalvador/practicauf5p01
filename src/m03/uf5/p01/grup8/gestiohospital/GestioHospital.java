/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m03.uf5.p01.grup8.gestiohospital;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;

/**
 *
 * @author Christian
 */
public class GestioHospital {

    public static void main(String[] args) {
        List<Pacient> pacient = new ArrayList<Pacient>();
        menu(pacient);
    }

    public static void menu(List<Pacient> pacient) {
        Scanner in = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("                            UF5P01 - GESTIO HOSPITAL                    ");
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("1. Registrar visita ");
            System.out.println("2. Nou pacient ");
            System.out.println("3. Mostrar pacient ");
            System.out.println("4. Mostrar metge ");
            System.out.println("5. Veure Historial ");
            System.out.println("6. Sortir ");
            System.out.println("");
            System.out.print("Escribe una de las opciones: ");
            opcion = in.nextInt();
            in.nextLine();
            System.out.println("");
            System.out.println("Has seleccionado la opcion " + opcion);
            switch (opcion) {
                case 1:
                    registrarVisita(pacient);
                    break;
                case 2:
                    nouPacient(pacient);
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    salir = true;
                    break;
                default:
                    System.out.println("Solo numeros entre 1 y 6");
            }

        }

    }

    public static void nouPacient(List<Pacient> pacient) {
        Scanner in = new Scanner(System.in);
        boolean salir = false;
        do {

            System.out.println("Nom del Pacient");
            String nom = in.nextLine();
            System.out.println("Cognom del Pacient");
            String cog1 = in.nextLine();
            System.out.println("Segon Cognom del Pacient");
            String cog2 = in.nextLine();
            System.out.println("Numero de la seguretar Social");
            String ss = in.nextLine();
            System.out.println("NIF");
            String nif = in.nextLine();
            System.out.println("Telefón");
            String telf = in.nextLine();
            Pacient afegirPacient = new Pacient(nom, cog1, cog2, ss, nif, telf);
            boolean existe;
            existe = Comprobar.comprobarNIF(nif, pacient);
            if (existe == true) {
                System.out.println("--------ATENCIO--------------");
                System.out.println("Aquest Usuari ja esta registrat");
                menu(pacient);
            } else {
                int index = Historial.returnInt(pacient);
                index = index - 1;
                System.out.println("INDEX " + index);
                pacient.add(index, afegirPacient);
            }

            System.out.println("Vols Crear un altre Pacient? SI/NO");
            String s = in.nextLine();
            if (s.equalsIgnoreCase("si")) {
                salir = false;
            } else {
                salir = true;
            }
        } while (salir == false);

        menu(pacient);
    }

    public static void registrarVisita(List<Pacient> pacient) {
        Scanner in = new Scanner(System.in);
        List<String> registrarVisita = new ArrayList();
        Pacient p;
        Metge m1;
        Malaltia m2;
        Visita v1;
        Historial h1;
        String option = "";
        System.out.println("Vols Crear un nou pacient? SI/NO");
        option = in.nextLine();
        if (option.equalsIgnoreCase("si")) {
            nouPacient(pacient);
        }
        System.out.println("Escull identificació del pacient:(1: NIF, 2.Seguretat Social, 3. Codi Historial)");
        int opcio = in.nextInt();
        if (opcio == 1) {
            in.nextLine();
            System.out.println("Introdueix NIF del pacient: ");
            String NIFp = in.nextLine();
            for (int i = 0; i < pacient.size(); i++) {
                if (pacient.get(i).getNIF().equalsIgnoreCase(NIFp)) {
                    Pacient.mostrarPacient(i, pacient);
                }
            }
        } else if (opcio == 2) {
            in.nextLine();
            System.out.println("Introdueix el numero de la seguretat social");
            String ss = in.nextLine();
            for (int i = 0; i < pacient.size(); i++) {
                if(pacient.get(i).getnumSegSocial().equalsIgnoreCase(ss)){
                    Pacient.mostrarPacient(i, pacient);
                }
                
            }

        } else if (opcio == 3) {
            in.nextLine();
            System.out.println("Introduiex el codi historial");
            int codi = in.nextInt();
                Pacient.mostrarPacient(codi, pacient);
            
        }

    }

    

}
